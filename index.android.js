import React, {
    Component } from 'react';
import  {
    AppRegistry
} from 'react-native';
import App from './components/App'
import { StackNavigator } from 'react-navigation';
import LoginScreen from './components/login/Login'
import SecondScreen from './components/SecondScreen'
import List from './components/list/List'
import SignUp from './components/signup/SignUp'
import Maps from './components/maps/Maps'

class TestProj extends Component {
    static navigationOptions = {
        title: 'Home Screen',
    };

    render(){
        const { navigation } = this.props;
        return (
            <App navigation={ navigation }/>
        );
    }
}

const Screens = StackNavigator({
    Home: { screen: TestProj },
    SecondScreen: { screen: SecondScreen },
    LoginScreen : { screen: LoginScreen},
    List : { screen: List},
    SignUp :{screen : SignUp},
    Maps :{screen : Maps}
});


AppRegistry.registerComponent('TestProj', () => Screens);
