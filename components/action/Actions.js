const actionTypes = {
    login: "login",
    main: "main",
    list: "list",
};

import Store from "../Redux";

const store = Store;
export class Actions {

    static Page() {
        return actionTypes;
    }

    static goToMain() {
        store.dispatch({
            type: "NAVIGATE",
            page: Actions.Page().main
        });
    }


    static goToLogin() {
        store.dispatch({
            type: "NAVIGATE",
            page: Actions.Page().login
        });
    }

    static goToList() {
        store.dispatch({
            type: "NAVIGATE",
            page: Actions.Page().list
        });
    }
}