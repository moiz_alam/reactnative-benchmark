/**
 * Created by Moiz on 8/11/2017.
 */
import React, { Component } from 'react';

import {
    Text,
    Picker,
    View,
    StyleSheet,
    TextInput,
    Button,
    Image,
    ToastAndroid,
    Switch
} from 'react-native'
import ImagePicker from 'react-native-image-picker';

var options = {
    title: 'Select Avatar',
    customButtons: [
        {name: 'fb', title: 'Choose Photo from Facebook'},
    ],
    storageOptions: {
        skipBackup: true,
        path: 'images'
    }
};

ImagePicker.showImagePicker(options, (response) => {
    console.log('Response = ', response);
    if (response.didCancel) {
        console.log('User cancelled image picker');
    }
    else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
    }
    else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
    }
    else {
        //let source = { uri: response.uri };

        // You can also display the image using data:
         let source = { uri: 'data:image/jpeg;base64,' + response.data };
        ToastAndroid.showWithGravity(response.data, ToastAndroid.SHORT, ToastAndroid.CENTER);

        this.setState({
            avatarSource: source
        });
    }
});

class SignUp extends Component {

    static navigationOptions = {
        title: 'SignUp',
    };

    constructor(props){
        super(props);

        this.state = {
            userhint: 'Enter UserName here',
            passwordhint: 'Enter Password here',
            emailhint: 'Enter Email Address here',
            country:'',
            switchValue: false

        };
    }

    updateCountry = (country) =>{
    this.setState({ country: country });
    ToastAndroid.showWithGravity(country, ToastAndroid.SHORT, ToastAndroid.CENTER);
    }

    toggleSwitch = (value) => this.setState({ switchValue: value })

    _openImageLib() {
        // Launch Camera:
        ImagePicker.launchImageLibrary(options, (response)  => {
            // Same code as in above section!
            let source = { uri: 'data:image/jpeg;base64,' + response.data };

            this.setState({
                avatarSource: source
            });
        });
    }

    _openCamera() {
        // Open Image Library:
        ImagePicker.launchCamera(options, (response)  => {
            // Same code as in above section!
            let source = { uri: response.uri };

            // You can also display the image using data:
            //let source = { uri: 'data:image/jpeg;base64,' + response.data };

            this.setState({
                avatarSource: source
            });
        });
    }



    render() {
        return (
            <View style = {styles.signupcontainer}>
                <View style={{ flex:1, backgroundColor: 'transparent' }}>
                    <Image
                        style={{width: 90, height: 90}}
                        source={this.state.avatarSource}
                    />
                    <Text style={{marginTop:15}}>UserName</Text>
                    <TextInput
                        style={styles.inputfield}
                        underlineColorAndroid='transparent'
                        placeholder={this.state.userhint}
                    />
                    <Text>Email</Text>
                    <TextInput
                        style={styles.inputfield}
                        underlineColorAndroid='transparent'
                        placeholder={this.state.emailhint}
                    />
                    <Text>Password</Text>
                    <TextInput
                        style={[styles.inputfield]}
                        underlineColorAndroid='transparent'
                        secureTextEntry={true}
                        placeholder={this.state.passwordhint}
                    />
                    <Text>Country</Text>
                    <Picker selectedValue = {this.state.user} onValueChange = {this.updateCountry}>
                        <Picker.Item label = "Pakistan" value = "1" />
                        <Picker.Item label = "USA" value = "2" />
                        <Picker.Item label = "Germany" value = "3" />
                    </Picker>
                    {/*<Text style = {styles.text}>{this.state.country}</Text>*/}

                    {/*<View style={styles.switchcontainer}>
                       <Switch style={{margin:0}} onValueChange = {this.toggleSwitch} value = {this.state.switchValue}/>
                        <Text style = {styles.text}>{this.state.switchValue ? 'I agree to the term of use' :
                            'I agree to the term of use'}</Text>
                    </View>*/}


                    <View style={{height:20}}/>
                    <Button
                        title="Register"
                    />
                    <View style={{height:20}}/>
                    <Button
                        onPress={() => this._openCamera()}
                        title="Camera"
                    />
                    <View style={{height:20}}/>
                    <Button
                        onPress={() => this._openImageLib()}
                        title="Image Gallery"
                    />
                </View>
            </View>
        )
    }
}

export default SignUp

const styles = StyleSheet.create ({
    signupcontainer: {
        flex:1,
        alignItems: 'center',
        backgroundColor: '#ede3f2',
    },inputfield:{
        width:300,
        height: 40,
        borderColor: 'white',
        borderWidth: 0,
        backgroundColor:'white',
        marginBottom:15
    },  text: {
        fontSize: 10,
        color: 'red',
    },
    switchcontainer: {
        flex: 1,
        flexDirection: 'row',
    },
})