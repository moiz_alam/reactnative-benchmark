'use strict';

import React, {Component} from 'react';

import {
    StyleSheet,
    View,
    Text,
    Image
} from 'react-native';

var styles = StyleSheet.create({
    titleText: {
        fontSize: 20,
        textAlign: 'center',
        color: 'white',
        padding: 20
    },
    termsText: {
        fontSize: 10,
        textAlign: 'center',
        color: 'white',
        padding: 10
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        width: null,
        height: null,
    },
    backdropView: {
        backgroundColor: 'rgba(0,0,0,0)',
    },
});

export class CardViewList extends Component {
    constructor(props){
        super(props);
    }

    render() {
        return (
            <Image source={require('./images/signup_bg.png')} style={styles.container}>
                <View style={styles.backdropView}>
                    <Text style={styles.titleText}>Spend Smart. Do More</Text>
                </View>
                <Image source={require('./images/signup_buckit_logo.png')} style={{padding: 15}}/>
                <View style={styles.backdropView}>
                    <Text style={styles.termsText}>By clicking signup you agree to our Terms of Service</Text>
                </View>
        </Image>);
    }
}
