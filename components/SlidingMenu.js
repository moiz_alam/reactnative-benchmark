/**
 * Created by moiz on 12/31/16.
 */
'use strict';

import React, {Component} from 'react';
import  {
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Image
} from 'react-native'


export class SlidingMenu extends Component {
    constructor(props) {
        super(props);

        console.log("=====>>Constructing SlidingMenu with [" + (this.props.openDrawer == undefined) + ":" +
            (this.props.closeDrawer == undefined) + "]");
    }

    _homePress() {
      alert('Home Click');
    }

    _aboutPress() {
        alert('About Us Click');
    }

    _helpPress() {
        alert('Help Click');
    }

    _exitPress() {
        alert('Exit Click');
    }

    render() {
        return (
            <View style={styles.containerdrawrer}>
               {/* <View style={styles.drawerHeader}>
                    <Text>Menu</Text>
                </View>*/}
                <ScrollView >
                    <View style={styles.containerDrawerItem}>
                    <Text  style={styles.drawerItemTitle}
                           onPress={() => {this.props.closeDrawer(); this._homePress()}}>Home</Text>
                    </View>
                    <View style={styles.hrLine}/>
                    <View style={styles.containerDrawerItem}>
                    <Text  style={styles.drawerItemTitle}
                           onPress={() => {this.props.closeDrawer(); this._aboutPress()}}>About Us</Text>
                    </View>
                    <View style={styles.hrLine}/>
                    <View style={styles.containerDrawerItem}>
                    <Text  style={styles.drawerItemTitle}
                           onPress={() => {this.props.closeDrawer(); this._helpPress()}}>Help</Text>
                    </View>
                    <View style={styles.hrLine}/>
                    <View style={styles.containerDrawerItem}>
                    <Text  style={styles.drawerItemTitle}
                           onPress={() => {this.props.closeDrawer(); this._exitPress()}}>Exit</Text>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    containerdrawrer: {
        flex: 1,
        backgroundColor: '#FFF'
    },
    containerDrawerItem: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        padding: 13
    },
    drawerHeader: {
        backgroundColor: '#50C7EF',
        height: 61,
    },
    hrLine: {
        backgroundColor: '#CCC',
        height: 1
    },
    drawerItemTitle: {
        color: '#6d6d6d',
        textAlign: 'center',
        fontSize: 15,
        marginLeft: 20

    }
})
