import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  Button,
  View
} from 'react-native';
import { StackNavigator } from 'react-navigation';
import {SlidingMenu} from "./SlidingMenu";
import Drawer from 'react-native-drawer'
import SplashScreen from 'react-native-splash-screen'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
let navigateDummy;

export class App extends Component {

    openDrawer() {
        if (this._drawer)
            this._drawer.open()
    }

    closeDrawer() {
        if (this._drawer)
            this._drawer.close();
    };

    constructor(props){
        super(props);
        SplashScreen.hide();
        navigateDummy = props.navigation;
    }

    renderControlPanel() {
        return (<SlidingMenu store={this.props.store}
                             openDrawer={this.openDrawer.bind(this)}
                             closeDrawer={this.closeDrawer.bind(this)}/>);
    }

    render() {
        const { navigate } = navigateDummy;

        return (
              <Drawer
                  ref={(ref) => this._drawer = ref}
                  open={false}
                  acceptTap={true}
                  openDrawerOffset={0.2}
                  panCloseMask={0.2}
                  closedDrawerOffset={-3}
                  tweenHandler={(ratio) => ({ main: { opacity:(2-ratio)/2 }})}
                  store={this.props.store}
                  content={this.renderControlPanel()}
              >
                <View style={styles.container}>
                  <Button
                      onPress={() => navigate('LoginScreen')}
                      title="Go to Login"
                  />
                  <View style={{height:20}}/>
                  <Button onPress={() => navigate('SignUp')}
                          title="Go to SignUp"
                  />
                    <View style={{height:20}}/>
                    <Button
                        title="Maps"
                        onPress={() => navigate('Maps')}
                    />
                  <View style={{height:20}}/>
                <Button
                    title="Open Drawer"
                    onPress={() => {this.openDrawer()}}
                />
                </View>
              </Drawer>
            );
    }
}
//navigate('Maps')

/*const App = (props)  => {
  const { navigate } = props.navigation;

  return (
    <View style={styles.container}>
      <Text style={styles.welcome}>
        App Template
      </Text>

      <Button
          onPress={() => navigate('LoginScreen')}
          title="Go to Login"
        />
    <View style={{height:20}}/>
      <Button onPress={() => navigate('Maps')}
          title="Go to SignUp"
      />
    </View>
  );
}*/

export default App
