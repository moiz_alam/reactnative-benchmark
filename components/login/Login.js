'use strict';

import React, {Component} from 'react';

import {
    StyleSheet,
    View,
    Text,
    Image,
    TextInput,
    Button,
    Dimensions,
    ScrollView,
    ActivityIndicator,

} from 'react-native';
import { StackNavigator } from 'react-navigation';


var styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        width: null,
        height: null,
    },inputfield:{
        width:300,
        height: 40,
        borderColor: 'white',
        borderWidth: 0,
        backgroundColor:'white',
        marginTop:20
    },marginBottom:{
        marginBottom:20
    },
    activityIndicator: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        height: 80
    }
});
let width = Dimensions.get('window').width;
var RANDOM_USERS = 'https://randomuser.me/api/?results=500';
export class Login extends Component {

    //closeActivityIndicator = () => setTimeout(() => this.setState({ animating: false }), 6000)


    static navigationOptions = {
        title: 'Login',
    };

    constructor(props){
        super(props);

        this.state = {
            userhint: 'Enter UserName here',
            passwordhint: 'Enter Password here',
            userError: 'UserName Required',
            passwordError: 'Password Required',
            username: '',
            password: '',
            isUserNameEmpty : false,
            isPasswordEmpty : false,

            animating : false
        };
    }

    fetchData(navigate) {
        fetch(RANDOM_USERS)
            .then((response) => response.json())
            .then((responseData) => {
               if(responseData.results){
                   this.setState({ animating: false });
                   navigate('List');
               }
               else
                   alert('Please check your internet connection.')
            })
            .done();
    }

    _handlePress() {
        const { navigate } = this.props.navigation;
        //alert('Welcome: ' + this.state.username);
        this.setState({ animating: false });
        this.fetchData(navigate);
    }

    _userNameChange(userText) {
        this.setState({username:userText});
        if(userText.length > 0)
            this.state.isUserNameEmpty = true;
        else
            this.state.isUserNameEmpty = false;

    }

    _passwordChange(passwordText) {
        this.setState({password:passwordText})
        if(passwordText.length > 0)
            this.state.isPasswordEmpty = true;
        else
            this.state.isPasswordEmpty = false;
    }

    render() {
        return (
                <Image source={require('./images/signup_bg.png')} style={styles.container}>
                <Image source={require('./images/loginuser.png')} />
            <View style={{ flex:1, backgroundColor: 'transparent' }}>
                <TextInput
                    style={styles.inputfield}
                    underlineColorAndroid='transparent'
                    onChangeText={(userText) => this._userNameChange(userText)}
                    placeholder={this.state.userhint}
                />
                {!this.state.isUserNameEmpty ? <Text style={{ color: 'red' }}>{this.state.userError}</Text> : null}
                <TextInput
                    style={[styles.inputfield]}
                    underlineColorAndroid='transparent'
                    secureTextEntry={true}
                    onChangeText={(passwordText) => this._passwordChange(passwordText)}
                    placeholder={this.state.passwordhint}
                />
                {!this.state.isPasswordEmpty ? <Text style={{ color: 'red' }}>{this.state.passwordError}</Text> : null}
                <View style={{height:20}}/>
                <Button
                    disabled={!this.state.isUserNameEmpty || !this.state.isPasswordEmpty}
                    onPress={() => this._handlePress()}
                    title="Login"
                />

                <ActivityIndicator
                    animating =  {this.state.animating}
                    color = '#fff'
                    size = "large"
                    style = {styles.activityIndicator}
                />
            </View>
                </Image>
        );
    }
}

Login.navigationOptions = {
    title: 'Login',
};

export default Login

