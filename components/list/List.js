'use strict';

import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    Image,
    ListView,
    TouchableHighlight,
    TouchableOpacity,
    Modal,
    ActivityIndicator
} from 'react-native';


export class List extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
                <View>
                    <RandomUsers/>
                </View>
        );
    }
}


var REQUEST_URL = 'https://www.googleapis.com/books/v1/volumes?q=subject:fiction';
var RANDOM_USERS = 'https://randomuser.me/api/?results=500';

class RandomUsers extends Component {
    constructor(props) {
        super(props);
        this.state = {

            animating : true,
            modalVisible: false,
            userImage: null,
            userName: null,
            userEmail:null,
            dataSource: new ListView.DataSource({
                rowHasChanged: (row1, row2) => row1 !== row2
            })
        };
    }

    toggleModal(visible) {
        this.setState({ modalVisible: visible});
    }


    componentDidMount() {
        this.fetchData();
    }

    fetchData() {
        fetch(RANDOM_USERS)
            .then((response) => response.json())
            .then((responseData) => {
                this.setState({
                    animating : false,
                    dataSource: this.state.dataSource.cloneWithRows(responseData.results),
                    isLoading: false
                });
            })
            .done();
    }
    render() {

        if (this.state.isLoading) {
            return this.renderLoadingView();
        }

        return (
        <View>
        <Modal animationType = {"slide"} transparent = {false}
               visible = {this.state.modalVisible}
               onRequestClose = {() => { console.log("Modal has been closed.") } }>
            <View style = {styles.modal}>
                <TouchableHighlight onPress={() => {this.toggleModal(!this.state.modalVisible)}}>
                    <Text style = {styles.text}>Close Modal</Text>
                </TouchableHighlight>
                <Text style={{margin:20}}>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the
                    industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type
                    and scrambled it to make a type specimen book. It has survived not only five centuries, but also the
                    leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with
                    the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing
                    software like Aldus PageMaker including versions of Lorem Ipsum.
                </Text>

            </View>

        </Modal>


        <ListView
                dataSource={this.state.dataSource}
                renderRow={this.renderNotice.bind(this)}
            />

            <ActivityIndicator
                animating =  {this.state.animating}
                color = '#000'
                size = "large"
                style = {styles.activityIndicator}
            />
        </View>
        );
    }
    renderNotice(book) {
        return (
            <TouchableHighlight  onPress={() => this._onPressItem(book)}>
                <View>
                    <View style={styles.bookcontainer}>
                        <Image
                            source={{ uri: book.picture.medium }}
                            style={styles.thumbnail} />
                        <View style={styles.rightContainer}>
                            <Text style={styles.title}>{book.login.username}</Text>
                            <Text style={styles.author}>{book.email}</Text>
                        </View>
                    </View>
                    <View style={styles.separator} />
                </View>
            </TouchableHighlight>
        );
    }

    _onPressItem(book) {
        //alert(book.email);
        this.toggleModal(true);
    }

    renderLoadingView() {
        return (
            <View style={styles.loading}>
                <Text style={styles.author}>
                    Loading books...
                </Text>
            </View>
        );
    }
}


var styles = StyleSheet.create({
    bookcontainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
        padding: 10,
    },
    thumbnail: {
        width: 53,
        height: 81,
        marginRight: 10,
    },
    rightContainer: {
        flex: 1,
    },
    title: {
        fontSize: 20,
        marginBottom: 8,
    },
    author: {
        color: '#F00',
    },
    separator: {
        height: 1,
        backgroundColor: '#dddddd',
    },
    listView: {
        backgroundColor: '#F5FCFF',
    },
    loading: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    modal: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#FFF',
    },
    text: {
        color: '#3f2949',
        marginTop: 10
    },activityIndicator: {
        marginTop:35,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        height: 80
    }
});

List.navigationOptions = {
    title: 'List',
};

export default List