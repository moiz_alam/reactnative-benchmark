/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */


// TODO : Unused Component
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TouchableHighlight
} from 'react-native';
import { StackNavigator } from 'react-navigation';
import {Login} from "./components/login/Login";


export default class TestProj extends Component {
    static navigationOptions = {
        title: 'Welcome',
    };

    constructor(props) {
        super(props);
        this.buttonPress = this.buttonPress.bind(this);
    }


    loginClick() {
        //const { navigate } = that.props.navigation;
        //this.props.navigator.navigate('LoginPage')
    };

  render() {
     // const { navigator } = this.props.navigation;
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          Welcome to hahaha!
        </Text>
        <TouchableHighlight onPress={() => this.loginClick()}>
          <Text style={styles.instructions}>
            Login
          </Text>
        </TouchableHighlight>
      </View>
    );
  }
}

const SimpleApp = StackNavigator({
    Home: { screen: TestProj },
    LoginPage: { screen: Login },
});


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
